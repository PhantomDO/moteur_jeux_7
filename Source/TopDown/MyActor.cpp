﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MyMeshComponent->SetupAttachment(RootComponent);
    const ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (CubeVisualAsset.Succeeded())
	{
		MyMeshComponent->SetStaticMesh(CubeVisualAsset.Object);
		MyMeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}

	const ConstructorHelpers::FObjectFinder<UMaterial> MeshMaterial(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	if (MeshMaterial.Succeeded())
	{
		MyMeshComponent->SetMaterial(0, MeshMaterial.Object);
	}

	MyParticleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MovementParticles"));
	MyParticleComponent->SetupAttachment(MyMeshComponent);

	const ConstructorHelpers::FObjectFinder<UParticleSystem> MovementParticle(TEXT("/Game/StarterContent/Particles/P_Steam_Lit"));
	if (MovementParticle.Succeeded())
	{
		MyParticleComponent->SetTemplate(MovementParticle.Object);
	}
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	const FString name = FString::Printf(TEXT("Actor_name_%ls"), *this->GetName());
	UE_LOG(LogActor, Warning, TEXT("%ls"), *name);
	UE_LOG(LogActor, Warning, TEXT("Actor_age_%d"), this->Age);
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SmokeEffect && !MyParticleComponent->IsActive())
	{
		MyParticleComponent->Activate();
	}
	else
	{
		MyParticleComponent->Deactivate();
	}

	if (!IsStatic)
	{
		INT32 random = FMath::RandRange(0, 10);

		FVector NewLocation = GetActorLocation();
		FRotator NewRotation = GetActorRotation();
		float RunningTime = GetGameTimeSinceCreation();
		float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime));
		NewLocation += MoveSpeed * FVector{ DeltaHeight * random, DeltaHeight * random, DeltaHeight * random };
		NewRotation += RotateSpeed * FRotator{ 0, DeltaHeight * random, 0 };

		SetActorLocation(NewLocation);
		SetActorRotation(NewRotation);
	}
}

bool AMyActor::MoveUp(float distance)
{
	FVector ActorLocation = GetActorLocation();
	ActorLocation += distance * FVector::UpVector;
	SetActorLocation(ActorLocation);
	return ActorLocation.Z > 0.0f;
}

