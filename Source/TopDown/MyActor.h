// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"

#include "MyActor.generated.h"



UCLASS()
class TOPDOWN_API AMyActor : public AActor
{
	GENERATED_BODY()
		
public:
	// Sets default values for this actor's properties
	AMyActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MyMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UParticleSystemComponent* MyParticleComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FlotingActor")
	float MoveSpeed = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlotingActor")
	float RotateSpeed = 150.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlotingActor")
	bool IsStatic = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlotingActor")
	bool SmokeEffect = false;

	INT32 Age = 20;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:

	UFUNCTION(BlueprintCallable, Category = "Character")
	bool MoveUp(float distance);

};
